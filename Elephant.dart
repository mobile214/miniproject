import 'Animal.dart';
class Elephant extends Animal {
  late String name;
  Elephant(String name) : super(name) {
    this.name = name;
  }

  void speak() {
    print("Pawoo! Pawoo! Pawoo!");
  }

  String getName() {
    return this.name;
  }

  void setName(String name) {
    this.name = name;
  }
}