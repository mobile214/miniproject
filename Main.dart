import 'dart:io';
import 'Monkey.dart';
import 'Elephant.dart';
import 'Quiz.dart';

void main(List<String> arguments) {
  String q1 =
      "Which fruit has the highest oil content?\n" + "1.Olive\n2.Avocado\n";
  String q2 = "Chinese Gooseberry is another name for:\n" + "1.Kiwi\n2.Lime\n";
  String q3 =
      "In the Hindu culture, the leaves of which fruit are hung at weddings to ensure fertility?\n" +
          "1.Guava\n2.Mango\n";
  String q4 =
      "Papain is a natural digestive aid that is found naturally in:\n" +
          "1.Papaya\n2.Cherry\n";
  String q5 = "Which of these fruits is not native to North America?\n" +
      "1.Pear\n2.Apple\n";
  String q6 =
      "There is a fruit juice that can increase the potency of some medication, even causing an overdose. Which fruit juice is this?\n" +
          "1.Pomegranate\n2.Grapefruit\n";
  String q7 = "Which hugely popular fruit is, in fact, a herb?\n" +
      "1.Banana\n2.Jackfruit\n";
  String q8 = "What is the only fruit to have its seeds on the outside?\n" +
      "1.Dragon fruit\n2.Strawberry\n";
  String q9 = "What command do you use to output data to the screen?\n" +
      "1.Watermelon\n2.Pumpkin\n";
  String q10 = "What command do you use to output data to the screen?\n" +
      "1.Coconut\n2.Mangosteen\n";

  List<Quiz> q = [
    new Quiz(q1, "1"),
    new Quiz(q2, "1"),
    new Quiz(q3, "2"),
    new Quiz(q4, "1"),
    new Quiz(q5, "2"),
    new Quiz(q6, "2"),
    new Quiz(q7, "1"),
    new Quiz(q8, "2"),
    new Quiz(q9, "2"),
    new Quiz(q10, "1"),
  ];

  showWelcome();
  showRules();

  print("You are a monkey! \nPlease input your name:");
  String player1 = stdin.readLineSync()!;
  Monkey monkey = new Monkey(player1);
  monkey.setName(player1);
  monkey.speak;

  print("--------------------------------------------");

  print("You are an elephant! \nPlease input your name:");
  String player2 = stdin.readLineSync()!;
  Elephant elephant = new Elephant(player2);
  elephant.setName(player2);
  elephant.speak();

  print("----------------------");
  print("Let's Start");
  print("----------------------");

  Quiz quiz = new Quiz("", "");
  quiz.process(q, player1, player2);

}

void showWelcome() {
  print("----------------------");
  print("Welcome to Fruit Quiz");
  print("----------------------");
}

void showRules() {
  print("How to play");
  print(".....................................................");
  print("This game need 2 players answer same 10 questions");
  print("Whoever has more points is won");
  print("Please input your choice just 1 or 2");
  print(".....................................................");
  print("Good Luck! , I hope you guys enjoy this game :)");
  print("--------------------------------------------------------");
}
