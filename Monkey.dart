import 'Animal.dart';

class Monkey extends Animal {
  late String name;

  Monkey(String name) : super(name) {
    this.name = name;
  }

  void speak() {
    print("Ook-ook! Eeek-aak-eek!");
  }

  String getName() {
    return this.name;
  }

  void setName(String name) {
    this.name = name;
  }
}