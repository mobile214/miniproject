import 'dart:io';

class Quiz {
  late String prompt;
  late String answer;

  Quiz(String prompt, String answer) {
    this.prompt = prompt;
    this.answer = answer;
  }

  String getAnswer() {
    return this.answer = answer;
  }

  int score1 = 0;
  int score2 = 0;
  void process(List<Quiz> q, String player1, String player2) {
    for (int i = 0; i < q.length; i++) {
      print(q[i].prompt);
      print("Monkey [$player1] turn:");
      String ans1 = stdin.readLineSync()!;
      print("Elephant [$player2] turn:");
      String ans2 = stdin.readLineSync()!;
      if (ans1 == q[i].getAnswer()) {
        score1++;
      }
      if (ans2 == q[i].getAnswer()) {
        score2++;
      }
    }

    print("-------------------------");
    print("Monkey score: $score1");
    print("Elephant score: $score2");
    print("-------------------------");

    if (score1>score2) {
      print("Monkey [$player1] is win");
    }else if (score2>score1) {
      print("Elephant [$player2] is win");
    }else{
      print("Draw!!");
    }
  }


}